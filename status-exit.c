#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

int main(int argc, char** argv) {
    XEvent e;
    Display* d;
    Window r;
    const char* an = "STATUS_EXIT";
    Atom a;

    if (argc >= 2) {
        fprintf(stderr, "Usage: status-exit\n");
        return 1;
    }

    if (!(d = XOpenDisplay(NULL))) {
        fprintf(stderr, "Error: failed to open display\n");
        return 1;
    }
    r = XDefaultRootWindow(d);
    if (!XInternAtoms(d, (char**)&an, 1, False, &a)) {
        fprintf(stderr, "Error: failed to get status atoms\n");
    } else {
        e.xclient.type = ClientMessage;
        e.xclient.window = None;
        e.xclient.message_type = a;
        e.xclient.format = 8;
        XSendEvent(d, r, False, SubstructureNotifyMask, &e);
    }

    XCloseDisplay(d);
    return 0;
}
