#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int main(int argc, char** argv) {
    Display* d;
    Window w;
    XWMHints* hints;
    int err = 1, set = 1;

    if (argc == 2 && (!strcmp(argv[1], "0") || !strcmp(argv[1], "off") || !strcmp(argv[1], "no"))) {
        set = 0;
    }
    if (!(d = XOpenDisplay(NULL))) {
        fputs("Error: unable to open display.\n", stderr);
        return 1;
    }
    w = DefaultRootWindow(d);
    if (!(hints = XGetWMHints(d, w)) && !(hints = XAllocWMHints())) {
        fputs("Error: unable to get window manager hints.\n", stderr);
    } else {
        if (set) {
            hints->flags |= XUrgencyHint;
        } else {
            hints->flags &= ~XUrgencyHint;
        }
        if (!XSetWMHints(d, w, hints)) {
            fputs("Error: unable to set urgency hint.\n", stderr);
        } else {
            err = 0;
        }
        XFree(hints);
    }
    XCloseDisplay(d);

    return err;
}
