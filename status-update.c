#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

int main(int argc, char** argv) {
    XEvent e;
    Display* d;
    Window r;
    const char* an = "STATUS_UPDATE";
    Atom a;
    size_t k;
    unsigned long part;

    if (argc != 3) {
        fprintf(stderr, "Usage: status-update part str\n");
        return 1;
    }
    part = strtoul(argv[1], NULL, 0);
    if (!part || part > 0xFFFF) {
        fprintf(stderr, "Error: invalid part number\n");
        return 1;
    }
    k = strlen(argv[2]) + 1;
    if (k > sizeof(e.xclient.data.b) - 2) {
        k = sizeof(e.xclient.data.b) - 2;
        fprintf(stderr, "Warning: truncating string\n");
    }

    if (!(d = XOpenDisplay(NULL))) {
        fprintf(stderr, "Error: failed to open display\n");
        return 1;
    }
    r = XDefaultRootWindow(d);
    if (!XInternAtoms(d, (char**)&an, 1, False, &a)) {
        fprintf(stderr, "Error: failed to get status atoms\n");
    } else {
        e.xclient.type = ClientMessage;
        e.xclient.window = None;
        e.xclient.message_type = a;
        e.xclient.format = 8;
        e.xclient.data.b[0] = part & 0xFFU;
        e.xclient.data.b[1] = (part >> 8U) & 0xFFU;
        memcpy(e.xclient.data.b + 2, argv[2], k);
        XSendEvent(d, r, False, SubstructureNotifyMask, &e);
    }

    XCloseDisplay(d);
    return 0;
}
