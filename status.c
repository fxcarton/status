#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <X11/Xlib.h>

void update_root_title(Display* d, Window r, char** parts, unsigned int numParts) {
    char *name, *ptr;
    size_t n = 0, k;
    unsigned int i;
    for (i = 0; i < numParts; i++) {
        if (!parts[i]) continue;
        k = strlen(parts[i]) + 1;
        if (n >= ((size_t)-1) - k) {
            fprintf(stderr, "Error: status is too big\n");
            return;
        }
        n += k;
    }
    if (!(name = malloc(n))) {
        fprintf(stderr, "Error: failed to allocate name\n");
        return;
    }
    ptr = name;
    for (i = 0; i < numParts; i++) {
        if (!parts[i]) continue;
        k = strlen(parts[i]);
        memcpy(ptr, parts[i], k);
        ptr[k] = '|';
        ptr += k + 1;
    }
    ptr[-1] = 0;
    XStoreName(d, r, name);
    free(name);
}

int main(void) {
    XEvent e;
    Display* d;
    Window r;
    const char* an[3] = {"STATUS_CREATE", "STATUS_UPDATE", "STATUS_EXIT"};
    Atom a[3];
    char** parts = NULL;
    unsigned int numParts = 0;
    pid_t child;

    fclose(stdout);
    if (!(d = XOpenDisplay(NULL))) {
        fprintf(stderr, "Error: failed to open display\n");
        return 1;
    }
    r = XDefaultRootWindow(d);
    XSelectInput(d, r, SubstructureNotifyMask);
    if (!XInternAtoms(d, (char**)an, 3, False, a)) {
        fprintf(stderr, "Error: failed to get status atoms\n");
    } else if ((child = fork()) == ((pid_t)-1)) {
        fprintf(stderr, "Error: failed to fork\n");
    } else if (child) {
        int status;
        return (waitpid(child, &status, 0) == child && WIFEXITED(status)) ? WEXITSTATUS(status) : 1;
    } else if (setsid() == ((pid_t)-1)) {
        fprintf(stderr, "Error: failed to setsid\n");
    } else if ((child = fork()) == ((pid_t)-1)) {
        fprintf(stderr, "Error: failed to fork\n");
    } else if (child) {
        printf("%lu\n", (unsigned long)child);
        return 0;
    } else for (;;) {
        XNextEvent(d, &e);
        if (e.type != ClientMessage || e.xclient.format != 8) continue;
        if (e.xclient.message_type == a[0]) {
            char** tmp;
            Window s = e.xclient.window;
            unsigned int r;
            if (numParts >= 0xFFFF || (numParts + 1) > ((unsigned int)-1) / sizeof(*parts)
             || !(tmp = realloc(parts, (numParts + 1) * sizeof(*parts)))) {
                fprintf(stderr, "Error: failed to create new part\n");
                r = 0;
            } else {
                parts = tmp;
                parts[numParts] = NULL;
                r = ++numParts;
            }
            e.xclient.window = r;
            e.xclient.format = 32;
            e.xclient.data.l[0] = r;
            XSendEvent(d, s, False, NoEventMask, &e);
        } else if (e.xclient.message_type == a[1]) {
            unsigned int part;
            part = ((unsigned)e.xclient.data.b[0])
                 | (((unsigned)e.xclient.data.b[1]) << 8);
            if (!part || part > numParts) continue;
            if (!parts[--part] && !(parts[part] = malloc(sizeof(e.xclient.data.b)))) continue;
            memcpy(parts[part], e.xclient.data.b + 2, sizeof(e.xclient.data.b) - 2);
            parts[part][sizeof(e.xclient.data.b) - 2] = 0;
            update_root_title(d, r, parts, numParts);
        } else if (e.xclient.message_type == a[2]) {
            break;
        }
    }

    XCloseDisplay(d);
    return 0;
}
