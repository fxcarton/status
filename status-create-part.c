#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

int main(int argc, char** argv) {
    XEvent e;
    Display* d;
    Window w, r;
    const char* an = "STATUS_CREATE";
    Atom a;

    if (argc >= 2) {
        fprintf(stderr, "Usage: status-create-part\n");
        return 1;
    }

    if (!(d = XOpenDisplay(NULL))) {
        fprintf(stderr, "Error: failed to open display\n");
        return 1;
    }
    r = XDefaultRootWindow(d);
    if (!XInternAtoms(d, (char**)&an, 1, False, &a)) {
        fprintf(stderr, "Error: failed to get status atoms\n");
    } else if (!(w = XCreateSimpleWindow(d, XDefaultRootWindow(d), 0, 0, 1, 1, 0, 0, 0))) {
        fprintf(stderr, "Error: failed to create window\n");
    } else {
        e.xclient.type = ClientMessage;
        e.xclient.window = w;
        e.xclient.message_type = a;
        e.xclient.format = 8;
        XSendEvent(d, r, False, SubstructureNotifyMask, &e);
        for (;;) {
            XNextEvent(d, &e);
            if (e.type != ClientMessage || e.xclient.format != 32 || e.xclient.message_type != a) continue;
            printf("%ld\n", (long)e.xclient.data.l[0]);
            break;
        }
        XDestroyWindow(d, w);
    }

    XCloseDisplay(d);
    return 0;
}
