CFLAGS ?= -Wall -O2 -march=native
DESTDIR ?=
PREFIX ?= $(HOME)/.local

PROGS := $(patsubst %.c,%,$(wildcard *.c))
DEPS := x11
CFLAGS += -std=c89 -pedantic
CFLAGS += $(shell pkg-config --cflags $(DEPS))
LDFLAGS += $(shell pkg-config --libs-only-L --libs-only-other $(DEPS))
LDLIBS += $(shell pkg-config --libs-only-l $(DEPS))

.PHONY: all clean install
all: $(PROGS)
clean:
	rm -f $(wildcard $(PROGS))
install: all
	mkdir -p $(if $(DESTDIR),$(DESTDIR)/)$(PREFIX)/bin
	cp $(PROGS) $(if $(DESTDIR),$(DESTDIR)/)$(PREFIX)/bin
